<?php
require_once 'app/init.php';

if(!empty($_POST))
{
    
    $validator->check($_POST, [
        'email' => [
            'required' => true,
            'maxlength' => 200,
            'email' => true,
            'unique' =>'users'
        ],
        'username' => [
            'required' => true,
            'maxlength' => 20,
            'minlength' => 3,
            'unique' =>'users'
        ],
        'password' => [
            'required' => true,
            'maxlength' => 255,
            'minlength' => 8
        ]
    ]);
    if($validator->fails()){
        // print_r($validator->errors()->all());
    }else{
        $email = $_POST['email'];
        $username = $_POST['username'];
        $password = $_POST['password'];

        $created = $auth->create([
            'email' => $email,
            'username' => $username,
            'password' => $password
        ]);

        if($created){
            header("Location: signin.php");
        }else{
            echo "There was some issue ehile creating your user!";
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Sign-up Page</title>
</head>
<body class="showcase">
    <div class="signup-content">
        <div class="signup-box">
            <form action="signup.php" method="POST">
                <h1>Sign Up</h1>
                <input type="text" placeholder="Enter Email" class="text-fields" name="email">
                <?php
                    if($validator->fails() && $validator->errors()->has('email')):
                ?>
                  <span class="validity-email"><?=$validator->errors()->first('email')?></span> 
                <?php
                    endif;
                ?> 
               
                <input type="text" placeholder="Enter Username" class="text-fields" name="username">
                <?php
                    if($validator->fails() && $validator->errors()->has('username')):
                ?>
                  <span class="validity-username"><?=$validator->errors()->first('username')?></span> 
                <?php
                    endif;
                ?> 
                <input type="password" placeholder="Enter Password" class="text-fields" name="password">
                <?php
                    if($validator->fails() && $validator->errors()->has('password')):
                ?>
                  <span class="validity-password"><?=$validator->errors()->first('password')?></span> 
                <?php
                    endif;
                ?> 
                <button class="button" type="submit">Sign up</button>
            </form>
        </div>
    </div>

</body>
</html>