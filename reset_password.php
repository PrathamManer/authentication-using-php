<?php

require 'app/init.php';
if(!empty($_GET['t']))
{
    $token = $_GET['t'];
    if($token && $tokenHandler->isValid($token,0)):


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Forgot Password</title>
</head>

<body class="showcase">
    <div class="Reset-Password-content">
        <div class="Reset-Password-box">
            <form action="reset_password.php" method="POST">
                <h1>Reset Password</h1>
                <input type="password" placeholder="Enter Your New Password" class="text-fields" name="password">
                <input type="hidden" name="t" value="<?= $token;?>">
                <button class="button" value="Reset Password" name="submit" type="submit">Submit</button>
            </form>
        </div>
    </div>

</body>

</html>

<?php
    else:
        echo "<h3> Invalid Link, please try with authenticated link!</h3>";
    endif;
}
//when i submit the form

else if(!empty($_POST))
{
    $password = $_POST['password'];
    $token = $_POST['t'];
    if($tokenHandler->isValid($token,0))
    {
        $user = $userHelper->findUserByToken($token);
        if($auth->updatePassword($user->id,$password))
        {
            $tokenHandler->deleteTokenByToken($token);
            echo "<h3>Password Reset Successfully</h3>";
            echo "<br><a href='signin.php'>Sign In</a>";
        }
        else
        {
            echo "Problem with server while updating password, please try again later!";
        }
    }
    else
    {
        echo "Your timeout , please try to generate a new link!";
    }
}
else
{
    echo "<h3>This looks like some fishy activity, we will report it to admin</h3>";
}