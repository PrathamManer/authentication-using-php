<?php
session_start();
error_reporting(E_ALL);
date_default_timezone_set('Asia/Kolkata');
$app = __DIR__;

require_once "{$app}/classes/ErrorHandler.php";
require_once "{$app}/classes/Database.php";
require_once "{$app}/classes/validator.php";
require_once "{$app}/classes/Auth.php";
require_once "{$app}/classes/Hash.php";
require_once "{$app}/classes/TokenHandler.php";
require_once "{$app}/classes/UserHelper.php";
require_once "{$app}/classes/MailConfigHelper.php";
$database = new Database();
$errorHandler = new ErrorHandler();
$validator = new Validator($database, $errorHandler);
$userHelper = new UserHelper($database);
$tokenHandler = new TokenHandler($database);
$auth = new Auth($database,$userHelper,$tokenHandler);
$mail = MailConfigHelper::getMailer();

$auth->build();
$tokenHandler->build();


// $auth->create([
//     'email' => 'Pratham22@gmail.com
//     'username' => Pratham
//     'password' => Pratham46
// ]);
