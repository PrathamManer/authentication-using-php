<?php
class Validator
{
    //created object of errorhandler 
    protected ErrorHandler $errorHandler;

    protected Database $database;

    //rules required for signing thing
    protected $rules = [
        'required',
        'minlength',
        'maxlength',
        'email',
        'unique'
    ];

    //joh message hum print karna jab voh rules invalidate hojaye toh
    protected $messages = [
        'required' => 'The :field field is required',
        'minlength' => 'The :field field must be minimun of :satisfier character',
        'maxlength' => 'The :field field must be maximum of :satisfier character',
        'email' => 'That is not a valid email address',
        'unique' => 'This :field is already taken!'
    ];

    //To access errrorHandler humne uska constructor use kiya
    public function __construct(Database $database,ErrorHandler $errorHandler) //dependecy injection
    {
        $this->database = $database;
        $this->errorHandler = $errorHandler;
    }

    public function errors(): ErrorHandler
    {
        return $this->errorHandler;
    }

    //rules invalidate hua toh error print karega
    public function fails(): bool
    {
        return $this->errorHandler->hasErrors();
    }

    //this function checks ki humare data mein ye sab rules hai ki nhi
    public function check($data, $rules)
    {
        foreach($data as $item => $value){
            //item humara 1 dimensional array isliye array_keys use kiya sirf rules ko access karne ke liye 
            if(in_array($item, array_keys($rules))){
                $this->validate($item,$value,$rules[$item]);
            }
        }
    }

    //field humara email element hai
    //value joh dalne wale hai agar pass 
    //rules joh humne field ke liye dale the
    public function validate($field,$value,$rules){
        foreach($rules as $rule => $satisfier) {
            if(in_array($rule,$this->rules)) {
                if(!call_user_func_array([$this,$rule],[$field,$value,$satisfier])){
                    $this->errorHandler->addError(
                        str_replace([':field',':satisfier'], [$field,$satisfier], $this->messages[$rule]), //humlogne yaha replace method use kiya so that so message print hoga 
                        //uska key lega voh aur satisfier pass hau toh ya it will print error
                        $field
                    );
                }
            }
        }
    }

    //this are method joh validate ke liye hai
    protected function required($field,$value,$satisfier): bool{
        return !empty(trim($value));   //for spaces we have trim the value if user space chodta hai toh
    }

    protected function minlength($field,$value,$satisfier) : bool{
        return mb_strlen(trim($value)) >= $satisfier;
    }

    protected function maxlength($field,$value,$satisfier) : bool{
        return mb_strlen(trim($value)) <= $satisfier;
    }
    protected function email($field,$value,$satisfier) : bool{
        return filter_var($value , FILTER_VALIDATE_EMAIL);
    }

    protected function unique($field, $value, $satisfier): bool
    {
        return ! $this->database
                      ->table($satisfier)
                      ->exists([$field=>$value]);
    }
}
?>
