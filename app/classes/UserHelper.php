<?php
class UserHelper
{
    private Database $database;
    public function __construct(Database $database)
    {
        $this->database = $database;
    }
    public function findUserByEmail(String $email): ?object
    {
        return $this->database
                    ->table('users')
                    ->where('email', '=', $email)
                    ->first();
    }

    public function findUserByUsername(String $username): ?object
    {
        return $this->database
                    ->table('users')
                    ->where('username', '=', $username)
                    ->first();
    }

    public function findUserByToken(String $token): ?object
    {
      $tokenObject =  $this->database
                    ->table('tokens')
                    ->where('token', '=', $token)
                    ->first();

        return $this->database
                    ->table('users')
                    ->where('id', '=', $tokenObject->user_id)
                    ->first();

    }
}