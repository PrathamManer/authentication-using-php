<?php
class Database{
    protected $host = 'localhost';
    protected $db = 'auth';
    protected $username='Pratham';
    protected $password='Pratham46';

    protected PDO $pdo;
    protected PDOStatement $stmt;
    protected string $table;
    protected bool $debug = true;

    public function __construct(){
        $dsn = "mysql:host={$this->host};dbname={$this->db}";
        try{
            $this->pdo = new PDO($dsn, $this->username, $this->password);

            if($this->debug){
                $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
        }catch(PDOException $e){
            die($this->debug ?  $e->getMessage() : 'Some Database Issue!');
        }
    }

    public function query(string $sql)
    {
        return $this->pdo->query($sql);
    }

    public function table(string $table): self
    {
        $this->table = $table;
        return $this;
    }

    public function fetchAll(string $sql)
    {
        return $this->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    public function where(string $field, string $operator, string $value)
    {
        $this->stmt = $this->pdo->prepare("SELECT * FROM {$this->table} WHERE {$field} {$operator} :value");
        $this->stmt->execute(['value' => $value]);
        return $this;
    }

    //We are keeping two ways to get the data that is fetchall() and get() method. If we want to access through object then we use get() and if we want to use in assoc mode then we'll use fetchAll()
    public function get()
    {
        return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function insert($data)
    {

        $keys = array_keys($data);  //This will store data in array
        $fields = "`" . implode("`, `", $keys) . "`";  //this is to make $keys array into string
        $placeholders = ":" . implode(", :", $keys);  //:username, :password,:email

        $sql = "INSERT INTO {$this->table}({$fields}) VALUES({$placeholders})";
        
        $this->stmt= $this->pdo->prepare($sql);
        return $this->stmt->execute($data);
    }

    public function count()
    {
        return $this->stmt->rowCount();
    }

    public function exists($data): bool
    {
        $field = array_keys($data)[0];
        return $this->where($field, "=", $data[$field])->count() ? true : false;
    }

    public function first()
    {
        return $this->get()[0];
    }

    public function deleteWhere($field, $value)
    {
        $sql = "DELETE FROM {$this->table} WHERE {$field} =:value";
        $this->stmt = $this->pdo->prepare($sql);
        $this->stmt->execute(['value'=>$value]);
        return $this;
    }
}