<?php
require('app/init.php');
if(!empty($_POST))
{
    $email = $_POST['email'];
    $user = $userHelper->findUserByEmail($email);
    if($user)
    {
        $tokenData = $tokenHandler->createForgotPasswordToken($user->id);
        if($tokenData)
        {
            $mail->addAddress($user->email);
            $mail->Subject = "Password Recovery!";
            $mail->Body = "Use this link within 10 minutes to reset the password: <br>";
            $mail->Body .= "<a href = 'http://localhost:9000/reset_password.php?t={$tokenData['token']}'>Reset Password</a>";
            if($mail->send())
            {
                echo "Please check your email to reset your password!";
            }
            else
            {
                echo "Problem sending mail, please try again later!";
            }
        }
        else
        {
            echo "<h3>There is some issue in server, please try again later!";
        }

    }
    else
    {
        echo "<h3>No such email id found!</h3>";
    }
}
else if($auth->check())
{
    echo "<h3>You are already signed in, how can you forget the password!</h3>";
}
else
{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Forgot Password</title>
</head>
<body class="showcase">
    <div class="Forgot-Password-content">
        <div class="Forgot-Password-box">
            <form action="forgot-password.php" method="POST">
                <h1>Forgot Password</h1>
                <input type="text" placeholder="Enter Your Email" class="text-fields" name="email">
                <button class="button" type="submit">Submit</button>
            </form>
        </div>
    </div>

</body>

</html>
<?php
}
?>