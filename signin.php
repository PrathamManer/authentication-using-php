<?php
require "app/init.php";
if(!empty($_POST))
{
    $username = $_POST['username'];
    $password = $_POST['password'];
    $rememberMe = $_POST['rem'];
    $status = $auth->signin($username, $password);
    if($status)
    {
        if($rememberMe)
        {
            $user = $userHelper->findUserByUsername($username);
            $token = $tokenHandler->createRememberMeToken($user->id);
            setcookie("token", $token['token'], time() + 1800);
        }
        header('Location: index.php');
    }
    else
    {
        echo "WRONG USERNAME/PASSWORD!";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Sign in Page</title>
</head>
<body>
<?php if(isset($_COOKIE['token']) && $tokenHandler->isValid($_COOKIE['token'], 1)): ?>

<h3> You are already signed in</h3>

<?php else: ?> 
    <div class="showcase">
        <div class="signin-content">
            <div class="signin-box">
                <form action="signin.php" method="POST">
                    <h1>Sign In</h1>
                    <input type="text" placeholder="Enter Username" class="text-fields" name="username">
                    <input type="password" placeholder="Enter Password" class="text-fields" name="password">
                    <label for="" class="remember-me"><input type="checkbox" name="rem" id="">Remember me</label>
                    <a href="forgot-password.php" class="forgot-password" name="forgot_password">Forgot password?</a>
                    <button class="button" type="submit" name="submit" value="Sign In">Sign in</button>
                    <br>
                    <a href="signup.php" class="create-account-link">Create Account</a>
                </form>
            </div>
        </div>
    </div>
<?php endif; ?>
</body>
</html>